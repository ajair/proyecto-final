#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#ifdef _WIN32
#else
#endif
#define MAX 80
#define VALOR_CENTINELA -1

struct producto {
	int codigo;
	char nombre[MAX];
	float costo;
	float precio;
	int cantidad;
};

typedef struct producto Producto;

Producto *obtenerProductos(int *n);
char existeProducto(int codigoProducto, Producto *producto);
char insertarProducto(Producto producto);
char eliminarProducto(int codigoProducto);
char eliminacionFisica();
char modificarProducto(Producto producto);
char guardarReporte();

int leecad(char *cad, int n);

/* Titular del programa */
void tituloPrincipal();
void tituloPrincipal()
{
	int i;
	printf("\n     ======================================================================\n");
	printf("\t    ESIME ZACATENCO - INGENIERIA EN CONTROL Y AUTOMATIZACION\n");
	printf("\t\t     PROYECTO FINAL - FUNDAMENTOS DE PROGRAMACION\n");
	printf("\t\t\t      DIAZ TORRES AXEL JAIR\n");
	printf("\t\t\t           GRUPO: 1AV7\n");
	printf("     ======================================================================\n");

	i = 0;
	putchar('\n');
	for (; i < 80; i++) {
		putchar('_');
	}
}
char linea[MAX];

int main(){

    int opciones;
    do{
    fflush(stdin);
    system("cls");
    tituloPrincipal();
    printf("\n\t\t=========> MENU PRICIPAL <============\n\n");

    printf("\t\t[1]. Stock\n");
    printf("\t\t[2]. Catalogo\n");
    printf("\t\t[3]. Salir\n");
    printf("\n\n\t\tElija una opcion: [ ]\b\b");
    scanf("%i",&opciones);

    switch (opciones){

        case 1:{

            char repite = 1;
            int opcion = -1;

            do {
                system("cls");

                tituloPrincipal();

                printf("\n\t\t============> BIENVENIDO <==============\n");
                printf("\n\t\t[1]. Insertar nuevo producto\n");
                printf("\t\t[2]. Mostrar listado de productos\n");
                printf("\t\t[3]. Eliminar un producto\n");
                printf("\t\t[4]. Buscar producto por clave\n");
                printf("\t\t[5]. Modificar un producto\n");
                printf("\t\t[6]. Eliminacion fisica de registros\n");
                printf("\t\t[7]. Salir\n");
                printf("\n\t\tIngrese su opcion: [ ]\b\b");

                leecad(linea, MAX);
                sscanf(linea, "%d", &opcion);

                switch (opcion) {

                    case 1:
                        {
                        Producto producto;
                        int codigoProducto = 0;
                        char repite = 1;
                        char respuesta[MAX];

                        do {
                        system("cls");
                        tituloPrincipal();
                        printf("\n\t\t\t==> INSERTAR PRODUCTO <==\n");

                        printf("\n\tCodigo de producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &codigoProducto);

                        if (!existeProducto(codigoProducto, &producto)) {

                        producto.codigo = codigoProducto;

                        printf("\tNombre del producto: ");
                        leecad(producto.nombre, MAX);

                        printf("\tCosto del producto ($): ");
                        leecad(linea, MAX);
                        sscanf(linea, "%f", &producto.costo);

                        printf("\tPrecio del producto ($): ");
                        leecad(linea, MAX);
                        sscanf(linea, "%f", &producto.precio);

                        printf("\tCantidad: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &producto.cantidad);

                        if (insertarProducto(producto)) {
                        printf("\n\tEl producto fue insertado correctamente\n");
                        } else {
                        printf("\n\tOcurrio un error al intentar insertar el producto\n");
                        printf("\tIntentelo mas tarde\n");
                        }
                        } else {
                        printf("\n\tEl producto de codigo %d ya existe.\n", codigoProducto);
                        printf("\tNo puede ingresar dos productos distintos con el mismo codigo.\n");
                        }
                        printf("\n\tDesea seguir ingresando productos? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (!(strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0)) {
                        repite = 0;
                        }

                        } while (repite);
                    }
                    break;

                    case 2:
                        {
                        Producto *productos;
                        int numeroProductos;
                        int i;
                        float costoTotal;
                        float precioTotal;
                        int cantidadTotal;
                        char respuesta[MAX];

                        system("cls");
                        tituloPrincipal();
                        productos = obtenerProductos(&numeroProductos);

                        if (numeroProductos == 0) {
                        printf("\n\tEl archivo esta vacio!!\n");
                        getchar();

                        } else {
                        printf("\n\t\t    ==> LISTADO DE PRODUCTOS REGISTRADOS <==\n");
                        printf(" ------------------------------------------------------------------------------\n");
                        printf("%8s\t%-20s%15s%15s%10s\n", "CODIGO", "NOMBRE", "COSTO $", "PRECIO $", "CANTIDAD");
                        printf(" ------------------------------------------------------------------------------\n");

                        costoTotal = 0;
                        precioTotal = 0;
                        cantidadTotal = 0;
                        for (i = 0; i < numeroProductos; i++) {
                        if (productos[i].codigo != VALOR_CENTINELA) {
                        printf("%7d \t%-20.20s%15.1f%15.1f%8d\n", productos[i].codigo, productos[i].nombre, productos[i].costo, productos[i].precio, productos[i].cantidad);
                        costoTotal += productos[i].costo;
                        precioTotal += productos[i].precio;
                        cantidadTotal += productos[i].cantidad;
                        }
                        }
                        printf(" ------------------------------------------------------------------------------\n");
                        printf("\t\t\t      TOTAL: %15.1f%15.1f%8d\n", costoTotal, precioTotal, cantidadTotal);

                        printf("\n\tDesea guardar el reporte en un archivo de texto? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        if (guardarReporte()) {
                        printf("\n\tEl reporte fue guardado con exito\n");
                        } else {
                        printf("\n\tOcurrio un error al guardar el reporte\n");
                        }

                        getchar();
                        }
                        }
                    }
                    break;

                    case 3:
                        {
                        Producto producto;
                        int codigoProducto;
                        char repite = 1;
                        char respuesta[MAX];

                        do {
                        system("cls");
                        tituloPrincipal();
                        printf("\n\t\t\t==> ELIMINAR PRODUCTO POR CODIGO <==\n");

                        printf("\n\tCodigo de producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &codigoProducto);

                        if (existeProducto(codigoProducto, &producto)) {

                        printf("\n\tCodigo del producto: %d\n", producto.codigo);
                        printf("\tNombre del producto: %s\n", producto.nombre);
                        printf("\tCosto del producto: %.1f $\n", producto.costo);
                        printf("\tPrecio del producto: %.1f $\n", producto.precio);
                        printf("\tCantidad: %d\n", producto.cantidad);

                        printf("\n\tSeguro que desea eliminar el producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        if (eliminarProducto(codigoProducto)) {
                        printf("\n\tProducto eliminado satisfactoriamente.\n");
                        } else {
                        printf("\n\tEl producto no pudo ser eliminado\n");
                        }
                        }

                        } else {
                        printf("\n\tEl producto de codigo %d no existe.\n", codigoProducto);
                        }
                        printf("\n\tDesea eliminar otro producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (!(strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0)) {
                        repite = 0;
                        }
                        } while (repite);
                    }
                    break;

                    case 4:
                        {
                        Producto producto;
                        int codigoProducto;
                        char repite = 1;
                        char respuesta[MAX];

                        do {
                        system("cls");
                        tituloPrincipal();
                        printf("\n\t\t\t==> BUSCAR PRODUCTO POR CODIGO <==\n");

                        printf("\n\tCodigo de producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &codigoProducto);

                        if (existeProducto(codigoProducto, &producto)) {

                        printf("\n\tCodigo del producto: %d\n", producto.codigo);
                        printf("\tNombre del producto: %s\n", producto.nombre);
                        printf("\tCosto del producto: %.1f $\n", producto.costo);
                        printf("\tPrecio del producto: %.1f $\n", producto.precio);
                        printf("\tCantidad: %d\n", producto.cantidad);

                        } else {
                        printf("\n\tEl producto de codigo %d no existe.\n", codigoProducto);
                        }
                        printf("\n\tDesea seguir buscando algun producto? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (!(strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0)) {
                        repite = 0;
                        }

                        } while (repite);
                    }
                    break;

                    case 5:
                        {
                        Producto producto;
                        int codigoProducto;
                        char repite = 1;
                        char respuesta[MAX];

                        do {
                        system("cls");
                        tituloPrincipal();
                        printf("\n\t\t\t==> MODIFICAR PRODUCTO POR CODIGO <==\n");

                        printf("\n\tCodigo de producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &codigoProducto);
                        if (existeProducto(codigoProducto, &producto)) {
                        printf("\n\tNombre del producto: %s\n", producto.nombre);
                        printf("\tCosto del producto ($): %.1f\n", producto.costo);
                        printf("\tPrecio del producto ($): %.1f\n", producto.precio);
                        printf("\tCantidad: %d\n", producto.cantidad);

                        printf("\n\tElija los datos a modificar\n");

                        printf("\n\tNombre del producto actual: %s\n", producto.nombre);
                        printf("\tDesea modificar el nombre del producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        printf("\tNuevo nombre del producto: ");
                        leecad(producto.nombre, MAX);
                        }
                        printf("\n\tCosto del producto actual: %.1f\n", producto.costo);
                        printf("\tDesea modificar el costo del producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        printf("\tNuevo costo del producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%f", &producto.costo);
                        }

                        printf("\n\tPrecio del producto actual: %.1f\n", producto.precio);
                        printf("\tDesea modificar el precio del producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        printf("\tNuevo precio del producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%f", &producto.precio);
                        }

                        printf("\n\tCantidad del producto actual: %d\n", producto.cantidad);
                        printf("\tDesea modificar la cantidad del producto? [S/N]: ");
                        leecad(respuesta, MAX);
                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        printf("\tNueva cantidad del producto: ");
                        leecad(linea, MAX);
                        sscanf(linea, "%d", &producto.cantidad);
                        }

                        printf("\n\tEsta seguro que desea modificar los datos del producto? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        if (modificarProducto(producto)) {
                        printf("\n\tEl producto fue modificado correctamente\n");

                        } else {
                        printf("\n\tOcurrio un error al intentar modificar el producto\n");
                        printf("\tIntentelo mas tarde\n");
                        }
                        }
                        } else {
                        printf("\n\tEl producto de codigo %d no existe.\n", codigoProducto);
                        }

                        printf("\n\tDesea modificar algun otro producto? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (!(strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0)) {
                        repite = 0;
                        }

                        } while (repite);
                    }
                    break;

                    case 6:
                        {
                        char respuesta[MAX];

                        system("cls");
                        tituloPrincipal();
                        printf("\n\t\t==> ELIMINAR FISICAMENTE REGISTROS DEL ARCHIVO <==\n");

                        printf("\n\tSeguro que desea proceder con la eliminacion fisica? [S/N]: ");
                        leecad(respuesta, MAX);

                        if (strcmp(respuesta, "S") == 0 || strcmp(respuesta, "s") == 0) {
                        if (eliminacionFisica()) {
                        printf("\n\tLa eliminacion fisica se realizo con exito.\n");
                        } else {
                        printf("\n\tOcurrio algun error en la eliminacion fisica.\n");
                        }

                        getchar();
                        }
                        }
                    break;

                    case 7:
                        repite = 0;
                    break;
                        }
            } while (repite);
        }
        break;

        case 2:{
            int catalogo;

            do{
            fflush(stdin);
            system("cls");
            tituloPrincipal();
            printf("\n\t\t==========> CATALOGO <==========\n\n");
            printf("\t\t[1]. Motores\n");
            printf("\t\t[2]. Transformadores\n");
            printf("\t\t[3]. Generadores\n");
            printf("\t\t[4]. Cable\n");
            printf("\t\t[5]. Salir\n");
            printf("\t\tElija una opcion: [ ]\b\b");
            scanf("%d",&catalogo);

            switch (catalogo) {
                case 1:{
                    int orden;
                    system("cls");
                    tituloPrincipal();
                    printf("\n\t\t==========> MOTORES <==========\n\n");
                    printf("\t\t[1]. Monofasico\n");
                    printf("\t\t[2]. Trifasico\n");
                    printf("\t\t[3]. Reductor\n");
                    printf("\t\t[4]. Velocidad variable\n");
                    //printf("\t\t[5]. Salir\n");
                     printf("\t\tPresione una tecla para continuar: [ ]\b\b");
                    scanf("%i",&orden);
                    }
                break;
                case 2: {
                    int orden;
                    system("cls");
                    tituloPrincipal();
                    printf("\n\t\t==========> TRANSFORMADORES <==========\n\n");
                    printf("\t\t[5]. De potencia\n");
                    printf("\t\t[6]. Electricos\n");
                    printf("\t\t[7]. Aislamiento\n");
                    //printf("\t\t[4]. Salir\n");
                     printf("\t\tPresione una tecla para continuar: [ ]\b\b");
                    scanf("%i",&orden);
                    }
                break;
                case 3: {
                    int orden;
                    system("cls");
                    tituloPrincipal();
                    printf("\n\t\t==========> GENERADORES <==========\n\n");
                    printf("\t\t[8]. Electromecanicos\n");
                    printf("\t\t[9]. Electroquimicos\n");
                    printf("\t\t[10]. Fotovoltaicos\n");
                   // printf("\t\t[4]. Salir\n");
                     printf("\t\tPresione una tecla para continuar: [ ]\b\b");
                    scanf("%i",&orden);
                    }
                break;
                case 4:{
                    int orden;
                    system("cls");
                    tituloPrincipal();
                    printf("\n\t\t==========> CABLE <==========\n\n");
                    printf("\t\t[11]. #10\n");
                    printf("\t\t[12]. #12\n");
                    printf("\t\t[13]. #16\n");
                   // printf("\t\t[4]. Salir\n");
                    printf("\t\tPresione una tecla para continuar: [ ]\b\b");
                    scanf("%i",&orden);
                    }
                break;
                case 5:
                break;
                default:
                {printf("\nOpcion invalida.\n");}
                break;}
            }while(catalogo!=5);
            }
            break;

        case 3:
        break;
        default:
        {printf("\nOpcion invalida.\n");}
        break;
        }

    }while(opciones!=3);

    getch();
    return 0;
    }

Producto *obtenerProductos(int *n)
{
	FILE *archivo;
	Producto producto;
	Producto *productos;
	int i;
	archivo = fopen("productos.dat", "rb");

	if (archivo == NULL) {
    *n = 0;
    productos = NULL;

	} else {

    fseek(archivo, 0, SEEK_END);
    *n = ftell(archivo) / sizeof(Producto);
    productos = (Producto *)malloc((*n) * sizeof(Producto));

    fseek(archivo, 0, SEEK_SET);
    fread(&producto, sizeof(producto), 1, archivo);
    i = 0;
    while (!feof(archivo)) {
    productos[i++] = producto;
    fread(&producto, sizeof(producto), 1, archivo);
    }

    fclose(archivo);
	}

	return productos;
}

char existeProducto(int codigoProducto, Producto *producto)
{
	FILE *archivo;
	char existe;

	archivo = fopen("productos.dat", "rb");

	if (archivo == NULL) {
		existe = 0;

	} else {
    existe = 0;

    fread(&(*producto), sizeof(*producto), 1, archivo);
    while (!feof(archivo)) {
    if ((*producto).codigo == codigoProducto) {
    existe = 1;
    break;
    }
    fread(&(*producto), sizeof(*producto), 1, archivo);
    }
    fclose(archivo);
	}

	return existe;
}

char insertarProducto(Producto producto)
{
	FILE *archivo;
	char insercion;

	archivo = fopen("productos.dat", "ab");

	if (archivo == NULL) {
    insercion = 0;

	} else {
    fwrite(&producto, sizeof(producto), 1, archivo);
    insercion = 1;

    fclose(archivo);
	}

	return insercion;
}

char eliminarProducto(int codigoProducto)
{
	FILE *archivo;
	FILE *auxiliar;
	Producto producto;
	char elimina;
	archivo = fopen("productos.dat", "r+b");

	if (archivo == NULL) {
    elimina = 0;

	} else {

    elimina = 0;
    fread(&producto, sizeof(producto), 1, archivo);
    while (!feof(archivo)) {
    if (producto.codigo == codigoProducto) {
    fseek(archivo, ftell(archivo) - sizeof(producto), SEEK_SET);
    producto.codigo = VALOR_CENTINELA;
    fwrite(&producto, sizeof(producto), 1, archivo);
    elimina = 1;
    break;
    }
    fread(&producto, sizeof(producto), 1, archivo);
    }
    fclose(archivo);
	}

	return elimina;
}

char eliminacionFisica()
{
	FILE *archivo;
	FILE *temporal;
	Producto producto;
	char elimina = 0;

	archivo = fopen("productos.dat", "rb");
	temporal = fopen("temporal.dat", "wb");

	if (archivo == NULL || temporal == NULL) {
    elimina = 0;
	} else {
    fread(&producto, sizeof(producto), 1, archivo);
    while (!feof(archivo)) {
    if (producto.codigo != VALOR_CENTINELA) {
    fwrite(&producto, sizeof(producto), 1, temporal);
    }
    fread(&producto, sizeof(producto), 1, archivo);
    }
    fclose(archivo);
    fclose(temporal);

    remove("productos.dat");
    rename("temporal.dat", "productos.dat");

    elimina = 1;
	}

	return elimina;
}

char modificarProducto(Producto producto)
{
	FILE *archivo;
	char modifica;
	Producto producto2;
	archivo = fopen("productos.dat", "rb+");

	if (archivo == NULL) {
    modifica = 0;

	} else {
    modifica = 0;
    fread(&producto2, sizeof(producto2), 1, archivo);
    while (!feof(archivo)) {
    if (producto2.codigo == producto.codigo) {
    fseek(archivo, ftell(archivo) - sizeof(producto), SEEK_SET);
    fwrite(&producto, sizeof(producto), 1, archivo);
    modifica = 1;
    break;
    }
    fread(&producto2, sizeof(producto2), 1, archivo);
    }

    fclose(archivo);
	}
	return modifica;
}

char guardarReporte()
{
	FILE *archivo;
	char guardado;
	Producto *productos;
	int numeroProductos;
	int i;
	float costoTotal;
	float precioTotal;
	int cantidadTotal;

	productos = obtenerProductos(&numeroProductos);

	if (numeroProductos == 0) {
    guardado = 0;

	} else {
    archivo = fopen("reporte.txt", "w");

    if (archivo == NULL) {
    guardado = 0;

    } else {
    fprintf(archivo, "\n\t\t    ==> LISTADO DE PRODUCTOS REGISTRADOS <==\n");
    fprintf(archivo, " ------------------------------------------------------------------------------\n");
    fprintf(archivo, "%8s\t%-20s%15s%15s%10s\n", "CODIGO", "NOMBRE", "COSTO $", "PRECIO $", "CANTIDAD");
    fprintf(archivo, " ------------------------------------------------------------------------------\n");

    costoTotal = 0;
    precioTotal = 0;
    cantidadTotal = 0;
    for (i = 0; i < numeroProductos; i++) {
    if (productos[i].codigo != VALOR_CENTINELA) {
    fprintf(archivo, "%7d \t%-20.20s%15.1f%15.1f%8d\n", productos[i].codigo, productos[i].nombre, productos[i].costo, productos[i].precio, productos[i].cantidad);
    costoTotal += productos[i].costo;
    precioTotal += productos[i].precio;
    cantidadTotal += productos[i].cantidad;
    }
    }
    fprintf(archivo, " ------------------------------------------------------------------------------\n");
    fprintf(archivo, "\t\t\t      TOTAL: %15.1f%15.1f%8d", costoTotal, precioTotal, cantidadTotal);

    guardado = 1;

    fclose(archivo);
    }
	}

	free(productos);
	productos = NULL;

	return guardado;
}

int leecad(char *cad, int n)
{
	int i, c;
	c = getchar();
	if (c == EOF) {
    cad[0] = '\0';
    return 0;
	}
	if (c == '\n') {
		i = 0;
	} else {
    cad[0] = c;
    i = 1;
	}

	for (; i < n - 1 && (c = getchar()) != EOF && c != '\n'; i++) {
    cad[i] = c;
	}
	cad[i] = '\0';

	if (c != '\n' && c != EOF)
    while ((c = getchar()) != '\n' && c != EOF);

	return 1;
}
